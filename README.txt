This code will look at entropy and entanglement in NISQ realizations of quantum algorithms, starting with Deutsch-Josza. There are a couple of things to work out...

1) What to simulate in Deutsch-Josza? What noise to put in? One good option would be to have an explicit experiment in mind.

2) How to measure entanglement along the way? A first goal will be to get time-dependence of entanglement. My understanding is that, in the absence of noise, the entanglement will peak midway through the computation, then come back to zero such that one measures the desired result.

Will also start to think about otehr algorithms that this could be applied to...

Deutsch-Josza may be a bad problem because it is efficiently solvable on a classical probabilistic computer (BPP). Alternative is, e.g., Simon's algorithm which distinguishes BPP and BQP.
